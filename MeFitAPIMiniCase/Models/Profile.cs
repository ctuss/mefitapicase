﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MeFitAPIMiniCase.Models
{
    public class Profile
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        public int? GoalId { get; set; }
        public Goal Goal { get; set; }
        public int? AddressId { get; set; }
        public Address Adress { get; set; }
        public int? ProgramId { get; set; }
        public Program Program { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        [MaxLength(100)]
        public string MedicalConditions { get; set; }
        public string Disabilities { get; set; }
    }
}
