﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitAPIMiniCase.Models
{
    public class Set
    {
        public int Id { get; set; }
        public int Repetitions { get; set; }
        public int Sets { get; set; }
        public ICollection<ExerciseSet> ExerciseSets { get; set; }
    }
}
