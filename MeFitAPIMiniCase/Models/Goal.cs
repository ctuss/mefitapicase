﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitAPIMiniCase.Models
{
    public class Goal
    {
        public int Id { get; set; }
        public string EndDate { get; set; }
        public bool Achieved { get; set; }
        public int? ProgramId { get; set; }
        public Program Program { get; set; }
    }
}
