﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitAPIMiniCase.Models
{
    public class MeFitDbContext : DbContext
    {
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<ProgramWorkout> ProgramWorkouts { get; set; }
        public DbSet<ExerciseSet> ExerciseSets { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Adresses { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public MeFitDbContext(DbContextOptions<MeFitDbContext> options): base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExerciseSet>().HasKey(pq => new { pq.ExerciseId, pq.SetId });
            modelBuilder.Entity<ProgramWorkout>().HasKey(pq => new { pq.ProgramId, pq.WorkoutId });

            modelBuilder.Entity<User>().HasData(new User { Id = 1, FirstName = "John", LastName = "Doe", Password = "Password", IsAdmin = false, IsContributor = false });

            modelBuilder.Entity<Profile>().HasData(new Profile { Id = 1, UserId = 1 });
        }
    }
}
