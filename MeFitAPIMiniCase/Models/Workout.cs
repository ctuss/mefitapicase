﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitAPIMiniCase.Models
{
    public class Workout
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Complete { get; set; }
        public List<ExerciseSet> ExerciseSets { get; set; }
        public ICollection<ProgramWorkout> ProgramWorkouts { get; set; }
    }
}
