﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitAPIMiniCase.Models
{
    public class ExerciseSet
    {
        public int ExerciseId { get; set; }
        public Exercise Exercise { get; set; }
        public int SetId { get; set; }
        public Set Set { get; set; }
    }
}
