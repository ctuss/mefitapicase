﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MeFitAPIMiniCase.Models;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPIMiniCase.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly MeFitDbContext _dbContext;

        public ProfileController(MeFitDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Profile>> Get()
        {
            return _dbContext.Profiles;
        }



        [HttpGet("{id}", Name = "Get")]
        public async Task<Profile> Get(int id)
        {
            var profile = await _dbContext.Profiles.Include("User").Include("Adress").Include("Program").SingleOrDefaultAsync(p => p.Id == id);
            return profile;
        }
    }
}