import React from 'react';
import Table from 'react-bootstrap/Table';

class ListProgramsComponent extends React.Component {
    state = {
        programs: null
    }
    async componentDidMount() {
        const url = 'https://localhost:5001/program';
        const response = await fetch(url);
        console.log(response);
        const json = await response.json();
        await this.setState({
            programs: json
        });
    }

    render() {
        if (this.state.programs != null) {
            return (
                <div id="tableDiv">
                    <div id="tableDivInner">
                        <Table striped bordered hover>
                            <thead id="tableHead">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Level</th>
                                    <th>Weeks</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.programs.map((program, i) =>
                                    <tr key={i} onClick={() => this.viewProgram(program)}>
                                        <td>{program.id}</td>
                                        <td>{program.user.firstName}</td>
                                        <td>{program.user.lastName}</td>
                                        <td>{program.height}</td>
                                        <td>{program.weight}</td>
                                        <tr>
                                            <div>test</div>
                                        </tr>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div>Loading</div>
            )
        }
    }
}

export default ListProgramsComponent;