import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import RegisterComponent from '../RegisterComponent/RegisterComponent';
import ProfileComponent from '../ProfileComponent/ProfileComponent';
import ListProfilesComponent from '../ListProfilesComponent/ListProfilesComponent';
import ListProgramsComponent from '../ListProgramsComponent/ListProgramsComponent'

import './DashboardComponent.css';

class DashboardComponent extends React.Component {
    render() {
        return (
            <div id="dashboardBody">
                <div id="dashboardNav">
                    <Navbar bg="light" variant="light">
                        <Navbar.Brand href="/">Training Application</Navbar.Brand>
                        <Nav className="mr-auto">
                            <Nav.Link href="/">Home</Nav.Link>
                            <Nav.Link href="/register">Register</Nav.Link>
                            <Nav.Link href="/programs">Programs</Nav.Link>
                        </Nav>
                    </Navbar>
                </div>
                <div id="dashboardBodyRouter">
                    <Router>
                        <Switch>
                            <Route exact path="/">
                                <ListProfilesComponent />
                            </Route>
                            <Route exact path="/register">
                                <RegisterComponent />
                            </Route>
                            <Route exact path="/profile/:id">
                                <ProfileComponent />
                            </Route>
                            <Route exact path="/programs">
                                <ListProgramsComponent/>
                            </Route>
                        </Switch>
                    </Router>
                </div>
            </div>
        )
    }
}

export default DashboardComponent;