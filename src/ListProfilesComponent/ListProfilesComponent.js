import React from 'react';
import Table from 'react-bootstrap/Table';
import { BrowserRouter as Router, Switch, Route, Link, withRouter } from 'react-router-dom';
import './ListProfilesComponent.css';

class ListProfilesComponent extends React.Component {
    state = {
        profiles: []
    }

    viewProfile(profile) {
        this.props.history.push('/profile/' + profile.id)
    }

    async componentDidMount() {
        const url = 'https://localhost:5001/profile';
        const response = await fetch(url);
        const json = await response.json();
        console.log(json);
        await this.setState({
            profiles: json
        });
    }
    render() {
        console.log(this.state.profiles);
        if (this.state.profiles != null) {
            return (
                <div id="tableDiv">
                    <div id="tableDivInner">
                        <Table striped bordered hover>
                            <thead id="tableHead">
                                <tr>
                                    <th>#</th>
                                    <th>First name</th>
                                    <th>Last name</th>
                                    <th>Height</th>
                                    <th>Weight</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.profiles.map((profile, i) =>
                                    <tr key={i} onClick={() => this.viewProfile(profile)}>
                                        <td>{profile.id}</td>
                                        <td>{profile.user.firstName}</td>
                                        <td>{profile.user.lastName}</td>
                                        <td>{profile.height}</td>
                                        <td>{profile.weight}</td>
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div>Loading</div>
            )
        }
    }
}

export default withRouter(ListProfilesComponent);