import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import DashboardComponent from './DashboardComponent/DashboardComponent';

function App() {
  return (
    <div className="App">
      <DashboardComponent />
    </div>
  );
}

export default App;
