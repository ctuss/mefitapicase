import React from 'react';
import { BrowserRouter as Router, Switch, Route, withRouter } from 'react-router-dom';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Grid, Row, Col } from 'react-bootstrap';

import './RegisterComponent.css';

class RegisterComponent extends React.Component {
    state = {
        profile: {}
    }

    async postProfile() {
        const url = 'https://localhost:5001/profile';
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.profile)
        });
        await response.json();
        await setTimeout(3000);
        this.props.history.push('/');
    }
    
    async registerProfile(e) {
        e.preventDefault();
        await this.setState({
            profile: {
                user: {
                    firstName: this.inputFirstName.value,
                    lastName: this.inputLastName.value,
                    isContributor: this.inputContributor.value === 'Yes' ? "true" : "false",
                    isAdmin: this.inputAdmin.value === 'Yes' ? "true" : "false",
                },
                adress: {
                    addressLine1: this.inputAddressLine1.value,
                    addressLine2: this.inputAddressLine2.value,
                    adressLine3: this.inputAddressLine3.value,
                    postalCode: this.inputZip.value,
                    city: this.inputCity.value,
                    country: this.inputCountry.value
                },
                weight: this.inputWeight.value,
                height: this.inputHeight.value,
                medicalConditions: this.inputMedicalConditions.value,
                disabilities: this.inputDisabilities.value
            }
        })
        console.log(this.inputAddressLine2.value);
        this.postProfile();
    }

    render() {
        console.log(this.state.profile)
        return (
            <div id="registerInput">
                <div id="registerBody">
                    <Form onSubmit={this.registerProfile.bind(this)}>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridFirstName">
                                <Form.Label>First name</Form.Label>
                                <Form.Control ref={input => this.inputFirstName = input} type="text" placeholder="Enter first name" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridLastName">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control ref={input => this.inputLastName = input} type="text" placeholder="Enter last name" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control ref={input => this.inputPassword = input} type="password" required placeholder="Enter last name" />
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridAddress1">
                                <Form.Label>Address</Form.Label>
                                <Form.Control ref={input => this.inputAddressLine1 = input} placeholder="1234 Main St" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridAddress2">
                                <Form.Label>Address 2</Form.Label>
                                <Form.Control ref={input => this.inputAddressLine2 = input} placeholder="Apartment, studio, or floor" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridAddress3">
                                <Form.Label>Address 3</Form.Label>
                                <Form.Control ref={input => this.inputAddressLine3 = input} placeholder="Apartment, studio, or floor" />
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridCity">
                                <Form.Label>City</Form.Label>
                                <Form.Control ref={input => this.inputCity = input} type="text" placeholder="City" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridZip">
                                <Form.Label>Zip</Form.Label>
                                <Form.Control ref={input => this.inputZip = input} type="number" placeholder="Zip code" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridCountry">
                                <Form.Label>Country</Form.Label>
                                <Form.Control ref={input => this.inputCountry = input} type="text" placeholder="Country" />
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridWeight">
                                <Form.Label>Weight</Form.Label>
                                <Form.Control ref={input => this.inputWeight = input} type="number" placeholder="Weight" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridHeight">
                                <Form.Label>Height</Form.Label>
                                <Form.Control ref={input => this.inputHeight = input} type="number" placeholder="Height" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridMedical">
                                <Form.Label>Medical conditions</Form.Label>
                                <Form.Control ref={input => this.inputMedicalConditions = input} type="text" placeholder="Conditions" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridDisabilities">
                                <Form.Label>Disabilities</Form.Label>
                                <Form.Control ref={input => this.inputDisabilities = input} type="text" placeholder="Disabilities" />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridContributor">
                                <Form.Label>Contributor</Form.Label>
                                <Form.Control ref={input => this.inputContributor = input} as="select">
                                    <option>Yes</option>
                                    <option>No</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridAdmin">
                                <Form.Label>Admin</Form.Label>
                                <Form.Control ref={input => this.inputAdmin = input} as="select">
                                    <option>Yes</option>
                                    <option>No</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group as={Col} id="registerButtonGroup" controlId="formGridRegister">
                                <Button variant="success" id="registerButton" type="submit">
                                    Register
                                </Button>
                            </Form.Group>
                        </Form.Row>
                    </Form>
                </div>
            </div >
        )
    }
}

export default withRouter(RegisterComponent);