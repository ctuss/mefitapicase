import React from 'react';
import { BrowserRouter as Router, Switch, Route, withRouter } from 'react-router-dom';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Grid, Row, Col } from 'react-bootstrap';

class ProfileComponent extends React.Component {
    state = {
        profile: {}
    }

    async componentDidMount() {
        const url = 'https://localhost:5001/profile/' + this.props.match.params.id;
        const response = await fetch(url);
        const json = await response.json();
        this.setState({
            profile: json
        })
    }

    async putProfile() {
        console.log("her e du no", this.state.profile);
        const url = 'https://localhost:5001/profile/' + this.props.match.params.id;
        const response = await fetch(url, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.profile)
        });
        console.log(response);
    }

    async removeProfile() {
        const url = 'https://localhost:5001/profile/' + this.props.match.params.id;
        const response = await fetch(url, {
            method: 'DELETE'
        });
        const json = await response.json();
        console.log(json);
        await setTimeout(3000);
        await this.props.history.push('/');
    }

    async updateProfile(e) {
        e.preventDefault();
        await this.setState({
            profile: {
                id: this.props.match.params.id,
                UserId: this.state.profile.user.id,
                user: {
                    id: this.state.profile.user.id,
                    firstName: this.inputFirstName.value,
                    lastName: this.inputLastName.value,
                    isContributor: this.inputContributor.value === 'Yes' ? "true" : "false",
                    isAdmin: this.inputAdmin.value === 'Yes' ? "true" : "false",
                },
                AddressId: this.state.profile.adress.id,
                adress: {
                    id: this.state.profile.adress.id,
                    addressLine1: this.inputAddressLine1.value,
                    addressLine2: this.inputAddressLine2.value,
                    adressLine3: this.inputAddressLine3.value,
                    postalCode: this.inputZip.value,
                    city: this.inputCity.value,
                    country: this.inputCountry.value
                },
                weight: this.inputWeight.value,
                height: this.inputHeight.value,
                medicalConditions: this.inputMedicalConditions.value,
                disabilities: this.inputDisabilities.value
            }
        })
        console.log(this.state.profile);
        this.putProfile();
    }

    render() {
        if (Object.keys(this.state.profile).length !== 0) {
            return (
                <div id="registerInput">
                    <div id="registerBody">
                        <Form onSubmit={this.updateProfile.bind(this)}>
                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridFirstName">
                                    <Form.Label>First name</Form.Label>
                                    <Form.Control ref={input => this.inputFirstName = input} type="text" defaultValue={this.state.profile.user.firstName} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridLastName">
                                    <Form.Label>Last name</Form.Label>
                                    <Form.Control ref={input => this.inputLastName = input} type="text" defaultValue={this.state.profile.user.lastName} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control ref={input => this.inputPassword = input} type="password" defaultValue={this.state.profile.user.password} />
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridAddress1">
                                    <Form.Label>Address</Form.Label>
                                    <Form.Control ref={input => this.inputAddressLine1 = input} defaultValue={this.state.profile.adress.addressLine1} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridAddress2">
                                    <Form.Label>Address 2</Form.Label>
                                    <Form.Control ref={input => this.inputAddressLine2 = input} defaultValue={this.state.profile.adress.addressLine2} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridAddress3">
                                    <Form.Label>Address 3</Form.Label>
                                    <Form.Control ref={input => this.inputAddressLine3 = input} defaultValue={this.state.profile.adress.addressLine3} />
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridCity">
                                    <Form.Label>City</Form.Label>
                                    <Form.Control ref={input => this.inputCity = input} type="text" defaultValue={this.state.profile.adress.city} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridZip">
                                    <Form.Label>Zip</Form.Label>
                                    <Form.Control ref={input => this.inputZip = input} type="number" defaultValue={this.state.profile.adress.postalCode} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridCountry">
                                    <Form.Label>Country</Form.Label>
                                    <Form.Control ref={input => this.inputCountry = input} type="text" defaultValue={this.state.profile.adress.country} />
                                </Form.Group>
                            </Form.Row>

                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridWeight">
                                    <Form.Label>Weight</Form.Label>
                                    <Form.Control ref={input => this.inputWeight = input} type="number" defaultValue={this.state.profile.weight} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridHeight">
                                    <Form.Label>Height</Form.Label>
                                    <Form.Control ref={input => this.inputHeight = input} type="number" defaultValue={this.state.profile.height} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridMedical">
                                    <Form.Label>Medical conditions</Form.Label>
                                    <Form.Control ref={input => this.inputMedicalConditions = input} type="text" defaultValue={this.state.profile.medicalConditions} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridDisabilities">
                                    <Form.Label>Disabilities</Form.Label>
                                    <Form.Control ref={input => this.inputDisabilities = input} type="text" defaultValue={this.state.profile.disabilities} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridContributor">
                                    <Form.Label>Contributor</Form.Label>
                                    <Form.Control ref={input => this.inputContributor = input} as="select" defaultValue={this.state.profile.isContributor ? "true" : "false"}>
                                        <option value="true">Yes</option>
                                        <option value="false">No</option>
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridAdmin">
                                    <Form.Label>Admin</Form.Label>
                                    <Form.Control ref={input => this.inputAdmin = input} as="select" defaultValue={this.state.profile.isAdmin ? "true" : "false"}>
                                        <option value="true">Yes</option>
                                        <option value="false">No</option>
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group as={Col} id="registerButtonGroup" controlId="formGridRegister">
                                    <Button variant="primary" id="registerButton" type="submit">
                                        Update
                                    </Button>
                                </Form.Group>
                                <Form.Group as={Col} id="removeButtonGroup" controlId="formGridRemove">
                                    <Button variant="danger" id="registerButton" type="button" onClick={() => this.removeProfile()}>
                                        Remove
                                    </Button>
                                </Form.Group>
                            </Form.Row>
                        </Form>
                    </div>
                </div >
            )
        }
        else {
            return (
                <div>Loading</div>
            )
        }
    }
}

export default withRouter(ProfileComponent);